# Laravel FE Integrator

Generate enums, routes, config from BE to FE for Laravel.

## Installation

1. Config in app.php ` Thainph\LaravelFeIntegrator\class LaravelFeIntegratorServiceProvider extends ServiceProvider,`
2. Publish config file: `php artisan vendor:publish --tag=laravel-fe-integrator`
## How to use
1. Generate enums:
- Run below command to generate enums to typescript enum:
    ```shell
    php artisan lfi:generate-enum {path-to-directory} --format={ts}
    ```
2. Generate routes: 
- Run below command to generate all routes to js or json:
    ```shell
    php artisan lfi:generate-route {path-to-directory} --format={js|json}
    ```
- Using with Ziggy:
    ```JS
    import route from "ziggy";
    import { Ziggy } from "{path-to-your-file}";
    
    Vue.mixin({
        methods: {
            route: (name, params, absolute) => route(name, params, absolute, Ziggy),
        }
    });
    
    
    ```
3. Generate config:
- Run below command to generate config to typescript enum:
    ```shell
    php artisan lfi:generate-config {path-to-directory} --format={ts}
    ```
