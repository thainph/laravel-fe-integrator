<?php

namespace Thainph\LaravelFeIntegrator\Console;

use Thainph\LaravelFeIntegrator\Helpers\Helper;
use Illuminate\Console\Command;

class GenerateConfigs extends Command
{
    protected $signature = 'lfi:generate-config {path} {--format=ts}';

    protected $description = 'Generate configs to typescript';

    public function handle(): void
    {
        if ($this->option('format') != 'ts') {
            $this->error('Invalid format! Only support ts.');
            return;
        }

        $targetDir = Helper::getTargetDirectory($this->argument('path'));
        $typescriptEnums = '';

        foreach (config('config-generator') as $key => $config) {
            $config = Helper::uppercaseArrayKeys($config);
            $name = Helper::snakeToPascal($key);
            $typescriptEnums .= Helper::getTypescriptEnumContent($name, $config);
        }

        $fileName = $targetDir . '/configs.' . $this->option('format');
        file_put_contents($fileName, $typescriptEnums);
        $this->info('Configs generated to ' . $fileName . '!');
    }
}
