<?php

namespace Thainph\LaravelFeIntegrator\Console;

use Illuminate\Console\Command;
use Thainph\LaravelFeIntegrator\Helpers\Helper;

class GenerateEnums extends Command
{
    protected $signature = 'lfi:generate-enum {path} {--format=ts}';

    protected $description = 'Generate enums to typescript enums and options';

    public function handle(): void
    {
        if ($this->option('format') != 'ts') {
            $this->error('Invalid format! Only support ts.');
            return;
        }

        $targetDir = Helper::getTargetDirectory($this->argument('path'));

        $enums = config('enum-generator.enums');
        $typescriptEnums = '';
        $typescriptOptions = "import { t } from '../plugins/i18n';\n";

        foreach ($enums as $enum) {
            $className = Helper::getEnumNameByClassName($enum);
            $typescriptEnums .= Helper::getTypescriptEnumContent($className, $enum::toArray());
            $typescriptOptions .= Helper::getTypescriptOptionContent($className, $enum::getSelectOptions());
        }

        file_put_contents("$targetDir/enums.ts", $typescriptEnums);
        file_put_contents("$targetDir/options.ts", $typescriptOptions);
        $this->info('Enums generated to ' . $targetDir . '/enums.ts and ' . $targetDir . '/options.ts!');
    }


}
