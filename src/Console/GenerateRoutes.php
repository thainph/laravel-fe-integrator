<?php

namespace Thainph\LaravelFeIntegrator\Console;

use Illuminate\Console\Command;
use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Reflector;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;
use Thainph\LaravelFeIntegrator\Helpers\Helper;

class GenerateRoutes extends Command
{
    protected $signature = 'lfi:generate-route {path} {--format=js}';

    protected $description = 'Generate routes to ziggy js';

    protected function isStartWithPrefixes($str, $prefixes = []): bool
    {
        foreach ($prefixes as $prefix) {
            if (Str::startsWith(rtrim(ltrim($str, '/'), '/'), rtrim(ltrim($prefix, '/'), '/'))) {
                return true;
            }
        }

        return false;
    }

    public function handle(): void
    {
        if (!in_array($this->option('format'), ['js', 'ts', 'json'])) {
            $this->error('Invalid format! Only support js, ts, json.');
            return;
        }

        $targetDirectory = Helper::getTargetDirectory($this->argument('path'));
        $domains = config('route-generator.domains');

        foreach ($domains as $name => $domain) {
            $data = [
                'url' => $domain['url'],
                'port' => null,
                'defaults' => [],
                'routes' => $this->getRoutes($domain)
            ];
            $format = $this->option('format');
            $fileName = $targetDirectory. '/' . $name . '.' . $format;

            switch ($format) {
                case 'js':
                case 'ts':
                    file_put_contents($fileName, $this->getJsContent(json_encode($data, JSON_PRETTY_PRINT)));
                    break;
                case 'json':
                    default:
                    file_put_contents($fileName, json_encode($data, JSON_PRETTY_PRINT));
                    break;
            }

            $this->info('Routes generated to '.$fileName.'!');
        }
    }
    protected function getRoutes($setting): array
    {
        $data = [];
        $routes = Route::getRoutes();

        foreach ($routes as $route) {
            if (empty($setting['prefixes']) || $this->isStartWithPrefixes($route->getPrefix(), $setting['prefixes'])) {

                $data[$route->getName()] = [
                    'uri' => $this->getUri($route, $setting),
                    'methods' => $route->methods(),
                    'bindings' => $this->getBindings($route)
                ];
            }
        }

        return $data;
    }

    protected function getUri($route, $setting): string
    {
        $uri = $route->uri();

        foreach ($setting['replaces'] as $item) {
            $uri = Str::replace($item['search'], $item['replace'], $uri);
        }

        return rtrim(ltrim($uri, '/'), '/');
    }

    protected function getBindings($route): array
    {
        $scopedBindings = method_exists(head($route) ?: '', 'bindingFields');

        $bindings = [];

        foreach ($route->signatureParameters(UrlRoutable::class) as $parameter) {
            if (! in_array($parameter->getName(), $route->parameterNames())) {
                break;
            }

            $model = class_exists(Reflector::class)
                ? Reflector::getParameterClassName($parameter)
                : $parameter->getType()->getName();
            $override = (new ReflectionClass($model))->isInstantiable()
                && (new ReflectionMethod($model, 'getRouteKeyName'))->class !== Model::class;

            // Avoid booting this model if it doesn't override the default route key name
            $bindings[$parameter->getName()] = $override ? app($model)->getRouteKeyName() : 'id';
        }

        return $scopedBindings ? array_merge($bindings, $route->bindingFields()) : $bindings;
    }

    protected function getJsContent($data): string
    {
        return <<<JAVASCRIT
const Routes = $data;

export {Routes};
JAVASCRIT;
    }
}
