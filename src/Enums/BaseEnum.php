<?php
namespace Thainph\LaravelFeIntegrator\Enums;

use Thainph\LaravelFeIntegrator\Helpers\Helper;
use MyCLabs\Enum\Enum;

class BaseEnum extends Enum
{
    public static function getClassName() {
        return get_called_class();
    }
    public static function getSelectOptions(): array
    {
        $options = [];
        $namespace = explode('\\', static::class);
        $className = array_pop($namespace);

        foreach (static::toArray() as $key => $value) {
            $keyI18n = config('enum-generator.i18n_prefix').Helper::camelToSnake($className).'.'.strtolower($key);
            $options[] = [
                'key' => $key,
                'value' => $value,
                'name' => __($keyI18n),
                'key_i18n' => $keyI18n
            ];
        }

        return $options;
    }

    public static function getNameByValue($targetValue, $locale = null)
    {
        $namespace = explode('\\', static::class);
        $className = array_pop($namespace);

        foreach (static::toArray() as $key => $value) {
            if ($targetValue === $value) {
                return  __(config('enum-generator.i18n_prefix').Helper::camelToSnake($className).'.'.strtolower($key), [], $locale);
            }
        }

        return null;
    }
}
