<?php
namespace Thainph\LaravelFeIntegrator\Helpers;

use Illuminate\Support\Str;

class Helper
{
    public static function getTargetDirectory(string $path): string
    {
        $targetDir = ltrim(rtrim($path));

        if (!file_exists($targetDir)) {
            mkdir($targetDir);
        }

        return $targetDir;
    }
    public static function camelToSnake(string $input): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }
    public static function snakeToPascal(string $input): string
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $input)));
    }
    public static function uppercaseArrayKeys(array $array): array
    {
        $result = [];

        foreach ($array as $key => $value) {
            $upperKey = strtoupper($key);

            if (is_array($value)) {
                $value = self::uppercaseArrayKeys($value);
            }

            $result[$upperKey] = $value;
        }

        return $result;
    }
    public static function getTypescriptEnumContent(string $name, array $data): string
    {
        $typescript = "export enum $name {\n";

        foreach ($data as $key => $value) {
            $value = is_string($value) ? "'$value'" : $value;
            $typescript .= "    $key = $value,\n";
        }

        $typescript .= "}\n";

        return $typescript;
    }
    public static function getTypescriptOptionContent(string $name, array $data): string
    {
        $typescript = "export const $name = [\n";

        foreach ($data as $value) {
            $realValue = is_string($value['value']) ? "'".$value['value']."'" : $value['value'];
            $option = "    {\n";
            $option .= "        value: $realValue,\n";
            $option .= "        name: t('".$value['key_i18n']."'),\n";
            $option .= "    },\n";
            $typescript .= $option;
        }

        $typescript .= "]\n";

        return $typescript;
    }
    public static function getEnumNameByClassName(string $className): string
    {
        $namespace = explode('\\', $className);
        return array_pop($namespace);
    }
}
