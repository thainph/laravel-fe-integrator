<?php
namespace Thainph\LaravelFeIntegrator;

use Illuminate\Support\ServiceProvider;
use Thainph\LaravelFeIntegrator\Console\GenerateRoutes;
use Thainph\LaravelFeIntegrator\Console\GenerateConfigs;
use Thainph\LaravelFeIntegrator\Console\GenerateEnums;
use Thainph\LaravelFeIntegrator\Console\GenerateI18n;

class LaravelFeIntegratorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/route-generator.php' => config_path('route-generator.php'),
            __DIR__.'/config/enum-generator.php' => config_path('enum-generator.php'),
            __DIR__.'/config/config-generator.php' => config_path('config-generator.php'),
        ], 'laravel-fe-integrator');


        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateRoutes::class,
                GenerateConfigs::class,
                GenerateEnums::class,
                GenerateI18n::class,
            ]);
        }

    }
}
