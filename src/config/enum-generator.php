<?php
return [
    // Include enums which you want to generate
   'enums' => [
       // 'App\Enums\ExampleEnum::class',
   ],
    // Prefix for i18n
    'i18n_prefix' => 'enum.',  // In this case you must create i18n file in resources/lang/{locale}/enum.php
];
