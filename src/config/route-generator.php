<?php
return [
    'domains' => [
        /* Example
         'api' => [ // Name of route file
            'url' => 'https://api.example.com', // Url of route file

            'prefixes' => [  // Prefixes of route which you want to generate
                'api/v1',
                'api/v2',
            ],

            'replaces' => [ // Replaces of route which you want to replace
                [
                    'search' => 'foo',
                    'replace' => 'bar',
                ]
            ]
        ],
        */
        'app' => [
            'url' => env('APP_URL'),
            'prefixes' => [],
            'replaces' => []
        ]
    ]
];
